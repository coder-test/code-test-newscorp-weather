const request = require('supertest');
const app = require('./app');

jest.mock('./geo', () => {
  return {
    forwardGeocoding: jest.fn().mockReturnValue(
      Promise.resolve({
        geometry: {},
        location: {
          city: 'sydney',
        },
      }),
    ),
  };
});

jest.mock('./openweathermapapi', () => {
  return {
    getCurrent: () =>
      Promise.resolve({
        data: {
          coord: { lon: 151.21, lat: -33.87 },
          weather: [{ id: 521, main: 'Rain', description: 'shower rain', icon: '09d' }],
          base: 'stations',
          main: {
            temp: 292.02,
            feels_like: 292.17,
            temp_min: 291.15,
            temp_max: 292.59,
            pressure: 1018,
            humidity: 93,
          },
          visibility: 10000,
          wind: { speed: 3.6, deg: 140 },
          rain: { '1h': 0.14 },
          clouds: { all: 75 },
          dt: 1602046343,
          sys: {
            type: 1,
            id: 9600,
            country: 'AU',
            sunrise: 1602012239,
            sunset: 1602057706,
          },
          timezone: 39600,
          id: 2147714,
          name: 'Sydney',
          cod: 200,
        },
      }),
    getForecastByGeometry: () =>
      Promise.resolve({
        data: {
          lat: 40.12,
          lon: -96.66,
          timezone: 'America/Chicago',
          timezone_offset: -18000,
          current: {
            dt: 1595243443,
            sunrise: 1595243663,
            sunset: 1595296278,
            temp: 293.28,
            feels_like: 293.82,
            pressure: 1016,
            humidity: 100,
            dew_point: 293.28,
            uvi: 10.64,
            clouds: 90,
            visibility: 10000,
            wind_speed: 4.6,
            wind_deg: 310,
            weather: [
              {
                id: 501,
                main: 'Rain',
                description: 'moderate rain',
                icon: '10n',
              },
              {
                id: 201,
                main: 'Thunderstorm',
                description: 'thunderstorm with rain',
                icon: '11n',
              },
            ],
            rain: {
              '1h': 2.93,
            },
          },

          daily: [
            {
              dt: 1595268000,
              sunrise: 1595243663,
              sunset: 1595296278,
              temp: {
                day: 298.82,
                min: 293.25,
                max: 301.9,
                night: 293.25,
                eve: 299.72,
                morn: 293.48,
              },
              feels_like: {
                day: 300.06,
                night: 292.46,
                eve: 300.87,
                morn: 293.75,
              },
              pressure: 1014,
              humidity: 82,
              dew_point: 295.52,
              wind_speed: 5.22,
              wind_deg: 146,
              weather: [
                {
                  id: 502,
                  main: 'Rain',
                  description: 'heavy intensity rain',
                  icon: '10d',
                },
              ],
              clouds: 97,
              pop: 1,
              rain: 12.57,
              uvi: 10.64,
            },
          ],
        },
      }),
  };
});

function getJson(url, body) {
  const httpRequest = request(app).get(url);
  httpRequest.set('Accept', 'application/json');
  return httpRequest;
}

describe('express app', () => {
  it('GET /weather/sydney JSON', async () => {
    const res = await request(app)
      .get('/weather/sydney')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);
    expect(res.body).toMatchSnapshot();
  });
  it('GET /weather/sydney HTML', async () => {
    const res = await request(app)
      .get('/weather/sydney')
      .set('Accept', 'text/html')
      .expect('Content-Type', /html/);
    expect(res.text).toMatchSnapshot();
  });

  it('GET /weather/sydney/today JSON', async (done) => {
    const res = await getJson('/weather/sydney/today');
    expect(res.body).toMatchSnapshot();
    done();
  });

  it('GET /weather/sydney/today html', async () => {
    const res = await request(app)
      .get('/weather/sydney/today')
      .set('Accept', 'text/html')
      .expect('Content-Type', /html/);
    expect(res.text).toMatchSnapshot();
  });

  it('GET /weather/sydney/monday HTML', async () => {
    const mockDate = new Date(1466424490000);

    const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
    const res = await request(app)
      .get('/weather/sydney/monday')
      .set('Accept', 'text/html')
      .expect('Content-Type', /html/);
    expect(res.text).toMatchSnapshot();
    spy.mockRestore();
  });

  it('GET /weather/sydney/monday JSON', async () => {
    const mockDate = new Date(1466424490000);

    const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
    const res = await request(app)
      .get('/weather/sydney/monday')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);
    expect(res.body).toMatchSnapshot();
    spy.mockRestore();
  });
});
