const axios = require('axios');

// http://api.weatherapi.com/v1/forecast.json?key=7490873fe8f6435d8f855353200710&q=sydney&days=7
const baseUrl = 'http://api.weatherapi.com/v1';

// const baseUrl = 'http://api.openweathermap.org/data/2.5';
const APIKEY = process.env.WEATHER_API_KEY;

const { OPENWEATHERMAP_API_KEY } = process.env;
const OPENWEATHERMAP_BASEURL = 'https://api.openweathermap.org/data/2.5';

module.exports = {
  getCurrent: (city) => {
    const apiUrl = `${OPENWEATHERMAP_BASEURL}/weather`;
    return axios.get(apiUrl, {
      params: {
        units: 'metric',
        appid: OPENWEATHERMAP_API_KEY,
        q: `${city},AU`,
      },
    });
  },

  getForecastByGeometry: ({ lat, lng }, exclude = 'hourly,minutely,current') => {
    const key = OPENWEATHERMAP_API_KEY;
    const apiUrl = `${OPENWEATHERMAP_BASEURL}/onecall`;
    const queryParams = {
      units: 'metric',
      lat,
      lon: lng,
      exclude,
      appid: key,
    };

    return axios.get(apiUrl, { params: queryParams });
  },
};
