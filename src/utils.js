const { get } = require('lodash');
const moment = require('moment');
const {
  getForecastByGeometry,
  getForecast,
  getCurrent,
} = require('./openweathermapapi');

const DATEFORMAT = 'YYYY-MM-DD';

const { forwardGeocoding } = require('./geo');

const accept = (format) => (req, res, next) => {
  return req.accepts(format) ? next() : next('route');
};

const WEEKDAYS = [
  'SUNDAY',
  'MONDAY',
  'TUESDAY',
  'WEDNESDAY',
  'THURSDAY',
  'FRIDAY',
  'SATURDAY',
];

const transformWeatherData = (data) => {
  if (!data) {
    return null;
  }
  return {
    ...data,
    date: moment.unix(data.dt).format(DATEFORMAT + ' dddd'),
  };
};

const getLookupDate = (weekday) => {
  const todayIndex = new Date().getDay();
  const weekdayIndex = WEEKDAYS.findIndex((item) => item === weekday.toUpperCase());

  let offset = 0;
  if (todayIndex === weekdayIndex) {
    // same weekday next week
    offset = 7;
  } else {
    offset =
      weekdayIndex > todayIndex
        ? weekdayIndex - todayIndex
        : 7 - todayIndex + weekdayIndex;
  }

  return moment().add(offset, 'days').format(DATEFORMAT);
};

const lookUpWeekdayFromForecast = async (query, weekday) => {
  if (!WEEKDAYS.includes(weekday.toUpperCase())) {
    throw new Error('Invalid weekday');
  }

  const { location, geometry } = await forwardGeocoding(query);

  const lookupDate = getLookupDate(weekday);

  const { data } = await getForecastByGeometry(geometry);
  const daily = get(data, 'daily');
  const forecast = daily.find((d) => {
    return moment.unix(d.dt).format(DATEFORMAT) === lookupDate;
  });
  return { weather: transformWeatherData(forecast), location };
};

module.exports = {
  DATEFORMAT,
  lookUpWeekdayFromForecast,
  accept,
};
