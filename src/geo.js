const axios = require('axios');
const { get } = require('lodash');

const baseUrl = 'https://api.opencagedata.com/geocode/v1';
const APIKEY = process.env.GEO_API_KEY;

module.exports = {
  forwardGeocoding: async (city) => {
    const apiUrl = `${baseUrl}/json?no_annotations=1&q=${city},au&key=${APIKEY}`;
    const { data } = await axios.get(apiUrl);
    const result = get(data, 'results[0]');
    return {
      geometry: result.geometry,
      location: result.components,
    };
  },
};
