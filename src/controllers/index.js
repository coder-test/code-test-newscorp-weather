const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.status(200).render('index.jinja2');
});

router.get('/health', (req, res) => {
  res.json({
    status: 'ok',
  });
});

module.exports = router;
