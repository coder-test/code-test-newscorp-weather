const express = require('express');
const moment = require('moment');
const { getForecastByGeometry, getCurrent } = require('../openweathermapapi');
const { forwardGeocoding } = require('../geo');
const { DATEFORMAT, accept, lookUpWeekdayFromForecast } = require('../utils');

const router = express.Router();

router.get('/weather/:query', accept('html'), async (req, res) => {
  const { query } = req.params || {};
  try {
    const { geometry, location } = await forwardGeocoding(query);
    const { data } = await getForecastByGeometry(geometry);
    res.status(200).render('forecast.jinja2', {
      location,
      weather: {
        ...data,
        daily: (data.daily || []).map((d) => ({
          ...d,
          date: moment.unix(d.dt).format(DATEFORMAT),
        })),
      },
    });
  } catch (ex) {
    console.error(ex);
    res.status(500).render('error.jinja2', { error: ex.message });
  }
});

router.get('/weather/:query', accept('json'), async (req, res) => {
  const { query } = req.params || {};
  try {
    const { geometry, location } = await forwardGeocoding(query);
    const { data } = await getForecastByGeometry(geometry);
    res.json({ weather: data, location });
  } catch (ex) {
    console.error(ex);
    res.status(500).render('error.jinja2', { error: ex.message });
  }
});

router.get('/weather/:query/today', accept('html'), async (req, res) => {
  const { query, weekday } = req.params || {};
  try {
    const { data } = await getCurrent(query);
    res.status(200).render('today.jinja2', {
      ...data,
      langs: {
        temp: 'temperature',
        feels_like: 'feels like',
        temp_min: 'low',
        temp_max: 'high',
        pressure: 'pressure',
        humidity: 'humidity',
      },
    });
  } catch (ex) {
    console.error(ex);
    res.status(500).render('error.jinja2', { error: ex.message });
  }
});

router.get('/weather/:query/today', accept('json'), async (req, res) => {
  const { query } = req.params || {};
  try {
    const { data } = await getCurrent(query);
    res.json(data);
  } catch (ex) {
    console.error(ex);
    res.status(500).render('error.jinja2', { error: ex.message });
  }
});

router.get('/weather/:query/:weekday', accept('html'), async (req, res) => {
  const { query, weekday } = req.params || {};
  try {
    const { weather, location } = await lookUpWeekdayFromForecast(query, weekday);
    res.status(200).render('weather.jinja2', {
      weather,
      location,
      langs: {
        day: 'Day',
        eve: 'Evening',
        night: 'Night',
        morn: 'Morning',
      },
    });
  } catch (ex) {
    console.error(ex);
    res.status(500).render('error.jinja2', { error: ex.message });
  }
});

router.get('/weather/:query/:weekday', accept('json'), async (req, res) => {
  const { query, weekday } = req.params || {};
  try {
    const data = await lookUpWeekdayFromForecast(query, weekday);
    res.json(data);
  } catch (ex) {
    console.error(ex);
    res.status(500).json({ error: ex.message });
  }
});

module.exports = router;
