const express = require('express');
const cors = require('cors');
const fg = require('fast-glob');
const path = require('path');
const nunjucks = require('nunjucks');

const currentDir = __dirname;

const getExpressApp = () => {
  const app = express();

  app.use(cors());
  app.use(express.json());
  app.use(express.static('public'));
  app.use(express.urlencoded({ extended: false }));

  nunjucks.configure(path.join(currentDir, 'views'), {
    autoescape: true,
    express: app,
    // watch: true,
  });

  const controllers = fg.sync('controllers/**', {
    onlyFiles: true,
    cwd: currentDir,
    deep: 1,
  });

  controllers.forEach((file) => {
    // eslint-disable-next-line global-require, import/no-dynamic-require
    const fn = require(path.join(currentDir, file));
    app.use(fn);
  });

  // middlewares must be loaded after controllers
  const middlewares = fg.sync('middlewares/**', {
    onlyFiles: true,
    cwd: currentDir,
    deep: 1,
  });

  middlewares.forEach((file) => {
    // eslint-disable-next-line global-require, import/no-dynamic-require
    const fn = require(path.join(currentDir, file));
    app.use(fn);
  });
  return app;
};

module.exports = getExpressApp();
